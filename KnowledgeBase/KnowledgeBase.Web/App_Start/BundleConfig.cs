﻿using System.Web;
using System.Web.Optimization;

namespace KnowledgeBase.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));
            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                        "~/Scripts/Kendo/kendo.all.min.js"
                        ));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
            bundles.Add(new StyleBundle("~/Content/Kendo").Include(
                    "~/Content/Kendo.black.min.css",
                    "~/Content/Kendo.blueopal.min.css",
                    "~/Content/Kendo.bootstrap.min.css",
                    "~/Content/Kendo/kendo.common-bootstrap.min.css",
                    "~/Content/Kendo/kendo.common.min.css",
                    "~/Content/Kendo/kendo.dataviz.black.min.css",
                    "~/Content/Kendo/kendo.dataviz.blueopal.min.css",
                    "~/Content/Kendo/kendo.dataviz.bootstrap.min.css",
                    "~/Content/Kendo/kendo.dataviz.default.min.css",
                    "~/Content/Kendo/kendo.dataviz.flat.min.css",
                    "~/Content/Kendo/kendo.dataviz.highcontrast.min.css",
                    "~/Content/Kendo/kendo.dataviz.metro.min.css",
                    "~/Content/Kendo/kendo.dataviz.metroblack.min.css",
                    "~/Content/Kendo/kendo.dataviz.min.css",
                    "~/Content/Kendo/kendo.dataviz.moonlight.min.css",
                    "~/Content/Kendo/kendo.dataviz.silver.min.css",
                    "~/Content/Kendo/kendo.dataviz.uniform.min.css",
                    "~/Content/Kendo/kendo.default.min.css",
                    "~/Content/Kendo/kendo.flat.min.css",
                    "~/Content/Kendo/kendo.highcontrast.min.css",
                    "~/Content/Kendo/kendo.metro.min.css",
                    "~/Content/Kendo/kendo.metroblack.min.css",
                    "~/Content/Kendo/kendo.mobile.all.min.css",
                    "~/Content/Kendo/kendo.mobile.android4.min.css",
                    "~/Content/Kendo/kendo.mobile.blackberry.min.css",
                    "~/Content/Kendo/kendo.mobile.common.min.css",
                    "~/Content/Kendo/kendo.mobile.flat.min.css",
                    "~/Content/Kendo/kendo.mobile.icenium.min.css",
                    "~/Content/Kendo/kendo.mobile.ios.min.css",
                    "~/Content/Kendo/kendo.mobile.meego.min.css",
                    "~/Content/Kendo/kendo.mobile.wp8.min.css",
                    "~/Content/Kendo/kendo.moonlight.min.css",
                    "~/Content/Kendo/kendo.rtl.min.css",
                    "~/Content/Kendo/kendo.silver.min.css",
                    "~/Content/Kendo/kendo.uniform.min.css"
                ));
        }
    }
}
