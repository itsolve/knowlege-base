﻿using Kendo.Mvc.UI;
using KnowledgeBase.BLL.Models;
using KnowledgeBase.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;


namespace KnowledgeBase.Web.Controllers
{
    public class HomeController : BaseController
    {
        //komenatrz michał
        public ActionResult Index()
        {
            ViewBag.Message = "Nowa zmiana RafaŁ";
            // var categories = UoW.CategoryRepo.GetRange();
            return View();
        }
        
        public JsonResult  GetCategories([DataSourceRequest] DataSourceRequest request)
        {
            var categories = UoW.CategoryRepo.GetRange();
    
            return Json(categories.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

    public ActionResult About()
        {
           
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}