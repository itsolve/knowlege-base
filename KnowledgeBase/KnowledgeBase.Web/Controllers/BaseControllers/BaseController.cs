﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KnowledgeBase.DAL;


namespace KnowledgeBase.Web.Controllers
{
    public class BaseController: Controller
    {
        public UnitOfWork UoW { get; }

        public BaseController()
        {
            UoW = new UnitOfWork();
        }

        protected override void Dispose(bool disposing)
        {
            UoW.Dispose();
            base.Dispose(disposing);
        }

    }
}