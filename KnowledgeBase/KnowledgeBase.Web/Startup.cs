﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KnowledgeBase.Web.Startup))]
namespace KnowledgeBase.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
