﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeBase.BLL.Models
{
    public class Comment : BaseEntity
    {
        [Key]
        public int Id { get; set; }
                     
        public int PostId { get; set; }

        [ForeignKey("PostId")]
        public virtual Post Post { get; set; }

        public string Content { get; set; }

        public string AuthorId { get; set; }

        public virtual IdentityUser Author { get; set; }


    }
}
