﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeBase.BLL.Models
{
    /// <summary>
    /// Base Entity is base class for EF entities. Provide two properies: CreationDate and Modification Date  
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// Creation Date  
        /// </summary>
        public DateTime? CreationDate { get; set; }


        /// <summary>
        /// Modification Date 
        /// </summary>
        public DateTime? ModificationDate { get; set;  }
    }
}
