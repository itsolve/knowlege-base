﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnowledgeBase.DAL.Repository;
using KnowledgeBase.BLL;
using KnowledgeBase.BLL.Models;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KnowledgeBase.DAL
{
    public sealed class UnitOfWork : IDisposable
    {
        private readonly KnowledgeBaseContext context;
        private bool isDisposed;

        private IRepo<Category> categoryRepo;
        private IRepo<Comment> commentRepo;
        private IRepo<File> fileRepo;
        private IRepo<Post> postRepo;
        private IRepo<Tag> tagsRepo;
        private IRepo<IdentityUser> userRepo;

        public IRepo<Category> CategoryRepo
        {
            get
            {
                return categoryRepo ??  new Repository<Category>(context);
            }
        }

        public IRepo<Comment> CommentRepo
        {
            get
            {
                return commentRepo?? new Repository<Comment>(context);
            }
            
        }

        public IRepo<File> FileRepo
        {
            get
            {
                return fileRepo?? new Repository<File>(context);
            }            
        }

        public IRepo<Post> PostRepo
        {
            get
            {
                return postRepo ?? new Repository<Post>(context);
            }
        }

        public IRepo<Tag> TagsRepo
        {
            get
            {
                return tagsRepo ?? new Repository<Tag>(context);
            }
        }

        public IRepo<IdentityUser> UserRepo
        {
            get
            {
                return userRepo?? new Repository<IdentityUser>(context);
            }
        }

        public UnitOfWork()
        {
            this.context = new KnowledgeBaseContext();
        }

        private void Dispose(bool disposing)
        {
            if (!this.isDisposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }

        public int SaveChanges()
        {
            try
            {
                try
                {
                    return context.SaveChanges();
                }
                catch (DbUpdateException ex)
                {
                    SqlException innerException = null;
                    Exception tmpException = ex ;
                    while (innerException == null && tmpException != null)
                    {
                        innerException = tmpException.InnerException as SqlException;
                        tmpException = tmpException.InnerException;
                    }
                    if (innerException != null && innerException.Number == 2601)
                    {
                        throw new Exception("Given field value already exists in the database.", new Exception(innerException.Message));
                    }
                    throw;
                }
            }
            catch (DbEntityValidationException ex)
            {
                var errorMessages = ex.EntityValidationErrors
                                        .SelectMany(x => x.ValidationErrors)
                                        .Select(x => x.ErrorMessage);
                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(ex.Message, " The Validation Errors: ", fullErrorMessage);
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);

            }
        }
    }
}
