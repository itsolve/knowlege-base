﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeBase.DAL.Repository
{
    public interface IModelReadable<T>
    {
        T Get(int id, params Expression<Func<T, object>>[] tablePredicate);

        T GetNoTracking(int id, params Expression<Func<T, object>>[] tablePredicate);

        IEnumerable<T> GetRange(Expression<Func<T, bool>> filterPredicate = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderByPredicate = null, params Expression<Func<T, object>>[] tablePredicate);

        IEnumerable<T> GetRangeNoTracking(Expression<Func<T, bool>> filterPredicate = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderbyPredicate = null, params Expression<Func<T, object>>[] tablePredicate);

        IEnumerable<T> GetRangeForIds(IEnumerable<int> ids, params Expression<Func<T, object>>[] tablePredicate);

        IEnumerable<T> GetRangeForIdsNoTracking(IEnumerable<int> ids, params Expression<Func<T, object>>[] tablePredicate);

        int Count(Expression<Func<T, bool>> predicate = null);

    }
}
