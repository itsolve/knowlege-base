namespace KnowledgeBase.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingNames : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.TagPost", name: "Tag_Id", newName: "TagId");
            RenameColumn(table: "dbo.TagPost", name: "Post_Id", newName: "PostId");
            RenameIndex(table: "dbo.TagPost", name: "IX_Post_Id", newName: "IX_PostId");
            RenameIndex(table: "dbo.TagPost", name: "IX_Tag_Id", newName: "IX_TagId");
            DropPrimaryKey("dbo.TagPost");
            AddPrimaryKey("dbo.TagPost", new[] { "PostId", "TagId" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.TagPost");
            AddPrimaryKey("dbo.TagPost", new[] { "Tag_Id", "Post_Id" });
            RenameIndex(table: "dbo.TagPost", name: "IX_TagId", newName: "IX_Tag_Id");
            RenameIndex(table: "dbo.TagPost", name: "IX_PostId", newName: "IX_Post_Id");
            RenameColumn(table: "dbo.TagPost", name: "PostId", newName: "Post_Id");
            RenameColumn(table: "dbo.TagPost", name: "TagId", newName: "Tag_Id");
        }
    }
}
