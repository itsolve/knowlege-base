namespace KnowledgeBase.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_References_Toautors : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ApplicationUser", newName: "IdentityUser");
            RenameColumn(table: "dbo.IdentityUserClaim", name: "ApplicationUser_Id", newName: "IdentityUser_Id");
            RenameColumn(table: "dbo.IdentityUserLogin", name: "ApplicationUser_Id", newName: "IdentityUser_Id");
            RenameColumn(table: "dbo.IdentityUserRole", name: "ApplicationUser_Id", newName: "IdentityUser_Id");
            RenameIndex(table: "dbo.IdentityUserClaim", name: "IX_ApplicationUser_Id", newName: "IX_IdentityUser_Id");
            RenameIndex(table: "dbo.IdentityUserLogin", name: "IX_ApplicationUser_Id", newName: "IX_IdentityUser_Id");
            RenameIndex(table: "dbo.IdentityUserRole", name: "IX_ApplicationUser_Id", newName: "IX_IdentityUser_Id");
            AddColumn("dbo.IdentityUser", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Comment", "AuthorId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Post", "AuthorId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Comment", "AuthorId");
            CreateIndex("dbo.Post", "AuthorId");
            AddForeignKey("dbo.Comment", "AuthorId", "dbo.IdentityUser", "Id");
            AddForeignKey("dbo.Post", "AuthorId", "dbo.IdentityUser", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Post", "AuthorId", "dbo.IdentityUser");
            DropForeignKey("dbo.Comment", "AuthorId", "dbo.IdentityUser");
            DropIndex("dbo.Post", new[] { "AuthorId" });
            DropIndex("dbo.Comment", new[] { "AuthorId" });
            AlterColumn("dbo.Post", "AuthorId", c => c.String());
            AlterColumn("dbo.Comment", "AuthorId", c => c.String());
            DropColumn("dbo.IdentityUser", "Discriminator");
            RenameIndex(table: "dbo.IdentityUserRole", name: "IX_IdentityUser_Id", newName: "IX_ApplicationUser_Id");
            RenameIndex(table: "dbo.IdentityUserLogin", name: "IX_IdentityUser_Id", newName: "IX_ApplicationUser_Id");
            RenameIndex(table: "dbo.IdentityUserClaim", name: "IX_IdentityUser_Id", newName: "IX_ApplicationUser_Id");
            RenameColumn(table: "dbo.IdentityUserRole", name: "IdentityUser_Id", newName: "ApplicationUser_Id");
            RenameColumn(table: "dbo.IdentityUserLogin", name: "IdentityUser_Id", newName: "ApplicationUser_Id");
            RenameColumn(table: "dbo.IdentityUserClaim", name: "IdentityUser_Id", newName: "ApplicationUser_Id");
            RenameTable(name: "dbo.IdentityUser", newName: "ApplicationUser");
        }
    }
}
