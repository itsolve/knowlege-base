﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using KnowledgeBase.BLL.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System;

namespace KnowledgeBase.DAL
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public partial class KnowledgeBaseContext : IdentityDbContext<ApplicationUser>
    {
        public KnowledgeBaseContext()
            : base("KnowledgeBaseConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<KnowledgeBaseContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<IdentityUserLogin>().HasKey<string>(l => l.UserId);
            modelBuilder.Entity<IdentityRole>().HasKey<string>(r => r.Id);
            modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });

            modelBuilder.Entity<Post>()
                .HasMany(p => p.Tags)
                .WithMany(t => t.Posts)
                .Map(mc =>
                {
                    mc.ToTable("TagPost");
                    mc.MapLeftKey("PostId");
                    mc.MapRightKey("TagId");
                });
        }

        public override int SaveChanges()
        {
            var entities = ChangeTracker.Entries().Where(en => en.Entity is BaseEntity && (en.State == EntityState.Added || en.State == EntityState.Modified));
            foreach (var entity in entities)
            {
                if (entity.State == EntityState.Added)
                {
                    ((BaseEntity)entity.Entity).CreationDate = DateTime.UtcNow;
                    ((BaseEntity)entity.Entity).ModificationDate = DateTime.UtcNow;
                }
                else
                {
                    entity.Property("CreationDate").IsModified = false;
                    ((BaseEntity)entity.Entity).ModificationDate = DateTime.UtcNow;
                }
            }

            return base.SaveChanges();
        }

        public static KnowledgeBaseContext Create()
        {

            return new KnowledgeBaseContext();
        }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<File> Files { get; set; }


    }
}