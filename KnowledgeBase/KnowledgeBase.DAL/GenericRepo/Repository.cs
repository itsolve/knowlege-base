﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Infrastructure;

namespace KnowledgeBase.DAL.Repository
{
    public class Repository<T> : IRepo<T> where T : class
    {
        protected readonly DbContext context;
        protected readonly DbSet<T> dbSet;

        public Repository(DbContext context)
        {
            this.context = context;
            this.dbSet = context.Set<T>();
        }

        public virtual T Add(T entity)
        {
            return dbSet.Add(entity);
        }

        public virtual void AddOrUpdate(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
                dbSet.AddOrUpdate(entity);
        }

        public virtual IEnumerable<T> AddRange(IEnumerable<T> entities)
        {
            return dbSet.AddRange(entities);
        }

        public virtual int Count(Expression<Func<T, bool>> predicate = null)
        {
            return predicate == null ? dbSet.Count() : dbSet.Count(predicate);
        }

        public virtual bool Delete(T entity)
        {
            try
            {
                if (context.Entry(entity).State == EntityState.Detached)
                {
                    dbSet.Attach(entity);
                }
                dbSet.Remove(entity);
            }
            catch (Exception)
            {
                return false;
            }
            return true;

        }

        public virtual bool DeleteRange(IEnumerable<T> entities)
        {
            try
            {
                foreach (var item in entities.Where(en => context.Entry(en).State == EntityState.Detached))
                    dbSet.Attach(item);
                dbSet.RemoveRange(entities);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public virtual T Get(int id, params Expression<Func<T, object>>[] tablePredicate)
        {
            DbQuery<T> query = dbSet;
            var param = Expression.Parameter(typeof(T));
            var lambda = Expression.Lambda<Func<T, bool>>(Expression.Equal(Expression.Property(param, "Id"), Expression.Constant(id)), param);
            if (tablePredicate != null)
            {
                foreach (var item in tablePredicate)
                    query = (DbQuery<T>)query.Include(item);
            }
            return query.Where(lambda).SingleOrDefault();
        }

        public virtual T GetNoTracking(int id, params Expression<Func<T, object>>[] tablePredicate)
        {
            DbQuery<T> query = dbSet.AsNoTracking();
            var param = Expression.Parameter(typeof(T));
            var lambda = Expression.Lambda<Func<T, bool>>(Expression.Equal(Expression.Property(param, "Id"), Expression.Constant(id)), param);
            if (tablePredicate != null)
            {
                foreach (var item in tablePredicate)
                    query = (DbQuery<T>)query.Include(item);
            }
            return query.Where(lambda).SingleOrDefault();
        }

        public virtual IEnumerable<T> GetRange(Expression<Func<T, bool>> filterPredicate = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderByPredicate = null, params Expression<Func<T, object>>[] tablePredicate)
        {
            IQueryable<T> query = dbSet;
            return GetRangePrivate(filterPredicate, orderByPredicate, tablePredicate, query);
        }

        public virtual IEnumerable<T> GetRangeForIds(IEnumerable<int> ids, params Expression<Func<T, object>>[] tablePredicate)
        {
            DbQuery<T> query = dbSet;
            return GetRangeForIdsPrivate(ids, tablePredicate, query);
        }

        public virtual IEnumerable<T> GetRangeForIdsNoTracking(IEnumerable<int> ids, params Expression<Func<T, object>>[] tablePredicate)
        {
            DbQuery<T> query = dbSet.AsNoTracking();
            return GetRangeForIdsPrivate(ids, tablePredicate, query);
        }

        public virtual IEnumerable<T> GetRangeNoTracking(Expression<Func<T, bool>> filterPredicate = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderbyPredicate = null, params Expression<Func<T, object>>[] tablePredicate)
        {
            DbQuery<T> query = dbSet.AsNoTracking();
            return GetRangePrivate(filterPredicate, orderbyPredicate, tablePredicate, query);
        }

        public virtual T Update(T entity)
        {
            T oldEntity = null;
            if (typeof(T).GetProperties().Any(p => p.Name == "Id"))
            {
                var id = (int)entity.GetType().GetProperty("Id").GetValue(entity);
                oldEntity = dbSet.Find(id);
            }
            if (oldEntity != null && context.Entry(oldEntity).State != EntityState.Detached)
            {
                context.Entry(oldEntity).State = EntityState.Detached;
            }
            dbSet.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public virtual IEnumerable<T> UpdateRange(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                if (context.Entry(entity).State == EntityState.Detached)
                {
                    dbSet.Attach(entity);
                }
                context.Entry(entity).State = EntityState.Modified;
            }
            return entities;
        }

        private static IEnumerable<T> GetRangePrivate(Expression<Func<T, bool>> filterPredicate, Func<IQueryable<T>, IOrderedQueryable<T>> orderbyPredicate, Expression<Func<T, object>>[] tablePredicate, IQueryable<T> query)
        {
            if (filterPredicate != null)
            {
                query = query.Where(filterPredicate);
            }

            if (tablePredicate != null)
            {
                foreach (var inc in tablePredicate)
                {
                    query = (DbQuery<T>)query.Include(inc);
                }
            }
            return orderbyPredicate?.Invoke(query).ToList() ?? query.ToList();
        }

        private static IEnumerable<T> GetRangeForIdsPrivate(IEnumerable<int> ids, Expression<Func<T, object>>[] tablePredicate, DbQuery<T> query)
        {
            List<int> idsList = new List<int>();
            idsList.AddRange(ids);
            var returnedEntities = new List<T>();
            try
            {
                var param = Expression.Parameter(typeof(T));
                BinaryExpression binaryExpression = null;
                int listCount = idsList.Count;
                bool endFlag = true;
                const int loopSteps = 400;
                do
                {
                    int loopCount;
                    if (listCount - loopSteps < 0)
                    {
                        loopCount = listCount;
                        endFlag = true;
                    }
                    else
                    {
                        listCount -= loopSteps;
                        loopCount = loopSteps;
                    }

                    for (int i = 0; i < loopCount; i++)
                    {
                        if (i == 0)
                        {
                            binaryExpression = Expression.Equal(Expression.Property(param, "Id"), Expression.Constant(idsList[i]));
                        }
                        else
                        {
                            if (binaryExpression != null)
                                binaryExpression = Expression.Or(binaryExpression, Expression.Equal(Expression.Property(param, "Id"), Expression.Constant(idsList[i])));
                        }
                    }
                    idsList.RemoveRange(0, loopCount);
                    if (binaryExpression != null)
                    {
                        var lambda = Expression.Lambda<Func<T, bool>>(binaryExpression, param);
                        if (tablePredicate != null)
                        {
                            query = tablePredicate
                                         .Aggregate(query, (current, inc) => (DbQuery<T>)current.Include(inc));
                        }
                        returnedEntities.AddRange(query.Where(lambda).ToList());
                    }
                }
                while (endFlag);
            }
            catch (Exception)
            {
                throw;
            }
            return returnedEntities;
        }
    }
}
